# Laravel RESTFUL API 

There are only one test model "Item" which have full c.r.u.d.
> http://localhost:8000/api/items

## Setup
I suppose you already have composer and Laravel installed.

- You need all the third party software
>composer install

.env file (Mac)
>cp .env.example .env

.env file(Windows)
>cp .env.example .env

- Key
> php artisan generate:key

- Database

After you've configured your project in the .env file simply type 
> php artisan migrate

### Enjoy!