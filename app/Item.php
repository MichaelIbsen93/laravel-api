<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = ['name', 'description', 'price'];

    protected $guarded = ['id', 'created_at'];
}
