<?php


namespace App\Http\Service\ItemService;


use App\Http\Repository\ItemRepository\ItemRepository;

class ItemService implements IItemService
{

    private $itemRepository;

    /**
     * ItemService constructor.
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    function getAll()
    {
        return $this->itemRepository->getAll();
    }

    function get(int $id)
    {
        return $this->itemRepository->get($id);
    }

    function create(array $attr)
    {
        return $this->itemRepository->create($attr);
    }

    function update(int $id, array $attr)
    {
        $item = $this->get($id);

        if ($item == null) {
            return null;
        }

        return $this->itemRepository->update($id, $attr);
    }

    function delete(int $id)
    {
        return $this->itemRepository->delete($id);
    }
}