<?php


namespace App\Http\Service;


interface IService
{
    function getAll();

    function get(int $id);

    function create(array $attr);

    function update(int $id, array $attr);

    function delete(int $id);
}